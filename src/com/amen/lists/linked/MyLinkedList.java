package com.amen.lists.linked;

/**
 * LinkedList'a nie sortowana.
 * @param <E> - typ parametrów które przechowuje lista
 */
public class MyLinkedList<E> {
    private ListElement<E> root;

    public MyLinkedList() {
        this.root = null;
    }

    /**
     * Dodaj element do listy.
     * @param data - nowy element.
     */
    public void addElement(E data) {
        ListElement<E> listElement = new ListElement<>(data);
        if (root == null) {
            root = listElement;
        } else {
            ListElement tmp = root;
            while (tmp.getNext() != null) {
                tmp = tmp.getNext();
            }

            tmp.setNext(listElement);
        }
    }

    /**
     * Wyświetla elementy listy.
     */
    public void printList() {
        ListElement<E> tmp = root;
        if (root == null) return;

        System.out.println(tmp.getData());
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.println(tmp.getData());
        }
    }

    /**
     * Usun element (ostatni) z listy. Usuniecie metoda 1 (z uzyciem wskaznika/referencji na poprzedni element).
     */
    public void removeLastVersionWithPrevious() {
        ListElement<E> tmp = root;
        if (root == null) return;

        if (tmp.getNext() != null) {      // wiecej niz 1 element na liscie
            ListElement<E> tmpPrev = root; // przedostatni element

            while (tmp.getNext() != null) {
                tmpPrev = tmp;
                tmp = tmp.getNext();
            }
            tmpPrev.setNext(null);
            // tmp - to ostatni element
        } else {
            root = null;
        }
    }

    /**
     * Usun element (ostatni) z listy. Usuniecie metoda 2 (z uzyciem sprawdzenia za-następnego elementu).
     */
    public void removeLast() {
        ListElement<E> tmp = root;
        if (root == null) return;

        if (tmp.getNext() != null) {
            while (tmp.getNext() != null && tmp.getNext().getNext() != null) {
                tmp = tmp.getNext();
            }

            tmp.setNext(null);
        } else if (tmp != null && tmp.getNext() == null) { // to znaczy ze jest tylko jeden
            // element root
            root = null;
        }
    }

    /**
     * Pobranie elementu o indeksie i'tym.
     * @param indeks - indeks elementu
     * @return zwraca alement na itym indeksie
     * @throws ArrayIndexOutOfBoundsException - kiedy przekraczamy za tablice. (szukamy nieistniejacego elementu)
     */
    public E get(int indeks) throws ArrayIndexOutOfBoundsException{
        if (root == null) throw new ArrayIndexOutOfBoundsException(); // brak elementow - blad

        ListElement<E> tmp = root;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && counter != indeks) {
            // dopoki istnieja elementy i nie dotarlismy do konkretnego numeru elementu powtarzaj petle
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        // jesli licznik != indeks (wyszlismy z petli z powodu braku elementow [tmp.getNext() != null]
        // a nie dlatego ze dotarlismy do konkretnego elementu
        if (counter != indeks) {
            throw new ArrayIndexOutOfBoundsException(); // blad
        }

        return tmp.getData(); // zwróć DANE ELEMENTU a nie sam element

        // przejście do n-tego elementu listy i zwrócenie go
        // wskazówka (zrób licznik przechodzenia przez pętlę while [zeby wiedziec na ktorym
        //                                                                  jestesmy elemencie)
        // wskazówka (nie zapomnij sprawdzić czy dany element istnieje na liście)
        // wskazówka pro: wiesz co powinno się wydarzyć jeśli wykroczysz poza zakres?

//        return ?;
    }

    /**
     * Usun element na indeksie itym.
     * @param indeks - indeks elementu do usuniecia.
     */
    public void removeElement(int indeks) {
        if (root == null) throw new ArrayIndexOutOfBoundsException();

        ListElement<E> tmp = root;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && ((counter+1) != indeks)) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        if((counter+1) == indeks){ // tmp to element przed elementem usuwanym
            if(tmp.getNext() == null){
                throw new ArrayIndexOutOfBoundsException();
            }
            tmp.setNext(tmp.getNext().getNext());
        }
    }

    /**
     * Oblicz rozmiar listy.
     * @return - zwraca rozmiar listy.
     */
    public int size() {
        if (root == null) return 0;

        // w przeciwnym razie

        ListElement<E> tmp = root;
        int counter = 1; // licznik elementow
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }

        return counter;
    }
}