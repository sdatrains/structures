package com.amen.lists.array;

public class MyArrayList {
    private Object[] array;
    private int numberOfElements;

    public MyArrayList() {
        this.array = new Object[20000];
        this.numberOfElements = 0;
    }

    // dodaje 1 element
    // numberOfElements = 1
    // array[0] = element
    // array[1] = null

    /**
     * sprawdz czy mamy jeszcze miejsce w tablicy
     * jesli mamy - umiesc
     * jesli nie mamy to stworz nowa tablice, przepisz elementy,
     * i dopisz nowy element
     *
     * @param data
     */
    public void addElement(Object data) {
        if (numberOfElements >= array.length) {
            // trzeba rozszerzyc
            Object[] newArray = new Object[array.length * 2];
            for (int i = 0; i < array.length; i++) { //przepisz elementy
                newArray[i] = array[i];
            }
            array = newArray;
        }
        array[numberOfElements++] = data;
    }

    public void removeElement(int indeks) {
        if (indeks >= numberOfElements) throw new ArrayIndexOutOfBoundsException();

        // przesuwamy elementy w tablicy w lewo
        for (int i = indeks; i < numberOfElements; i++) {
            array[i] = array[i + 1];
        }

        numberOfElements--;
    }

    public void printList() {
        for (int i = 0; i < numberOfElements; i++) {
//            if (array[i] != null)
            System.out.println(array[i]);
        }
    }

    public void removeLast() {
        if (numberOfElements > 0)
            numberOfElements--;
    }

    public Object get(int indeks) {
        if (indeks >= numberOfElements) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return array[indeks];
    }

    public int size() {
        return numberOfElements;
    }
}