package com.amen.lists.sorted;

public class ListElement<E extends Comparable> {
    private E data;
    private ListElement<E> next;

    public ListElement(E data) {
        this.data = data;
        this.next = null;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public ListElement<E> getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }
}